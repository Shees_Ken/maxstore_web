
// Declare All the Components Here

Vue.component('home-component', require('./components/HomeComponent.vue').default);
Vue.component('top-header', require('./components/header/TopHeaderComponent.vue').default);
Vue.component('main-header', require('./components/header/MainHeaderComponent.vue').default);
Vue.component('search-bar', require('./components/header/searchbar/SearchBarComponent.vue').default);
Vue.component('banner', require('./components/banner/BannerComponent.vue').default);
Vue.component('category-list', require('./components/category/CategoryListComponent.vue').default);
Vue.component('item-box', require('./components/item-box/ItemBoxComponent.vue').default);
Vue.component('item-wrapper', require('./components/item-wrapper/ItemWrapperComponent.vue').default);
